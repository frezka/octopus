// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class OctopusEditorTarget : TargetRules
{
	public OctopusEditorTarget(TargetInfo Target)
	{
		Type = TargetType.Editor;
        ExtraModuleNames.Add("Octopus");
    }
}
