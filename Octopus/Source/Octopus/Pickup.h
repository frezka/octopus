// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Interactable.h"
#include "Pickup.generated.h"

/**
 * 
 */
UCLASS()
class OCTOPUS_API APickup : public AInteractable
{
	GENERATED_BODY()
	
public:
	APickup();

	UFUNCTION(BlueprintCallable)
	virtual FString GetPreInteractText() const override;
	virtual void Interact(class AMyPlayerController* pController) override;

protected:

	UPROPERTY(EditAnywhere)
	int32 Id;

	UPROPERTY(EditAnywhere)
	int32 Count;

	UPROPERTY(EditAnywhere)
	FString Name;
};
