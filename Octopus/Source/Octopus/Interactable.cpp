// Fill out your copyright notice in the Description page of Project Settings.

#include "Interactable.h"
#include "Octopus.h"
#include "MyPlayerController.h"
#include "OctopusCharacter.h"

// Sets default values
AInteractable::AInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject <USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("InteractMesh"));
	//FAttachmentTransformRules rules(EAttachmentRule::SnapToTarget, false);
	//Mesh->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	Mesh->SetupAttachment(RootComponent);
	CollisionBox = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionBox"));
	//CollisionBox->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	CollisionBox->SetupAttachment(RootComponent);
	CollisionBox->bGenerateOverlapEvents = true;
	
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AInteractable::OnPlayerEnterCollisionBox);
	CollisionBox->OnComponentEndOverlap.AddDynamic(this, &AInteractable::OnPlayerLeaveCollisionBox);
}

void AInteractable::Interact(AMyPlayerController* pController)
{
	InteractBP(pController);
	pController->UpdateCurrentInteractable();
}

void AInteractable::OnPlayerEnterCollisionBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this && OtherComp)
	{
		if (AOctopusCharacter* pCharacter = Cast<AOctopusCharacter>(OtherActor))
		{
			if (AMyPlayerController* pController = Cast<AMyPlayerController>(pCharacter->GetController()))
			{
				pController->AddInteractable(this);
				OnPlayerEnterCollisionBox(pController);
			}	
		}
	}
}

void AInteractable::OnPlayerLeaveCollisionBox(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && OtherActor != this && OtherComp)
	{
		if (AOctopusCharacter* pCharacter = Cast<AOctopusCharacter>(OtherActor))
		{
			if (AMyPlayerController* pController = Cast<AMyPlayerController>(pCharacter->GetController()))
			{
				pController->RemoveInteractable(this);
				OnPlayerLeaveCollisionBox();
			}
		}
	}
}
