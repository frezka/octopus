// Fill out your copyright notice in the Description page of Project Settings.

#include "Door.h"
#include "Octopus.h"
#include "MyPlayerController.h"



ADoor::ADoor() : CurrentPlayer(nullptr)
{
	NeededItemId = 0;
	Count = 5;
}

FString ADoor::GetPostInteractText() const
{
	if (CurrentPlayer)
		return FString::Printf(TEXT("You need %d more %ss"), CurrentPlayer->GetItemCountDifference(NeededItemId, Count), *Name);
	return FString();
}

void ADoor::OnPlayerEnterCollisionBox(class AMyPlayerController* pController)
{
	if (pController)
	{
		if (pController->GetItemCountDifference(NeededItemId, Count) > 0)
			CurrentPlayer = pController;
		else
		{
			pController->UpdateItemCount(NeededItemId, -Count);
			pController->RemoveInteractable(this);
			CollisionBox->bGenerateOverlapEvents = false;
			Open();
		}
	}
}

void ADoor::OnPlayerLeaveCollisionBox()
{
	CurrentPlayer = nullptr;
}
