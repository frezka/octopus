// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayerController.h"
#include "Octopus.h"
#include "Interactable.h"



const int32 PICKUP_COUNT = 2;

AMyPlayerController::AMyPlayerController() : CurrentInteractable(nullptr)
{
	Inventory.Init(0, PICKUP_COUNT);
}

void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("Use", IE_Pressed, this, &AMyPlayerController::Interact);
}

void AMyPlayerController::AddInteractable(AInteractable* pInteractable)
{
	SurroundingInteractable.Add(pInteractable);
	UpdateCurrentInteractable();
}

void AMyPlayerController::RemoveInteractable(AInteractable* pInteractable)
{
	SurroundingInteractable.RemoveAllSwap([pInteractable](AInteractable* pVal) { return pVal == nullptr || pVal == pInteractable; });
	UpdateCurrentInteractable();
}

AInteractable* AMyPlayerController::GetCurrentInteractable() const
{
	return CurrentInteractable;
}

void AMyPlayerController::UpdateCurrentInteractable()
{
	CurrentInteractable = nullptr;
	float minDist = 1000000;
	float curDist;
	if (APawn* pPawn = GetPawn())
	{
		for (auto& pInteractable : SurroundingInteractable)
		{
			if (pInteractable)
			{
				curDist = pPawn->GetDistanceTo(pInteractable);
				if (curDist < minDist)
				{
					curDist = minDist;
					CurrentInteractable = pInteractable;
				}
			}
		}
	}
}

void AMyPlayerController::Interact()
{
	if (CurrentInteractable)
	{
		CurrentInteractable->Interact(this);
	}
}

void AMyPlayerController::UpdateItemCount(int32 id, int32 value)
{
	if (Inventory.IsValidIndex(id))
		Inventory[id] += value;
}

int32 AMyPlayerController::GetItemCountDifference(int32 id, int32 value) const
{
	if (Inventory.IsValidIndex(id))
		return value - Inventory[id];
	return 0;
}

int32 AMyPlayerController::GetItemCount(int32 id) const
{
	if (Inventory.IsValidIndex(id))
		return Inventory[id];
	return 0;
}
