// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"


/**
 * 
 */
UCLASS()
class OCTOPUS_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	class AInteractable* CurrentInteractable;
	//index = id, value = count;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	TArray<int32> Inventory;

	TArray<AInteractable*> SurroundingInteractable;

public:
	AMyPlayerController();
	
	virtual void SetupInputComponent() override;

	void AddInteractable(AInteractable* pInteractable);
	void RemoveInteractable(AInteractable* pInteractable);
	UFUNCTION(BlueprintCallable)
	AInteractable* GetCurrentInteractable() const;
	void UpdateCurrentInteractable();
	void Interact();

	UFUNCTION(BlueprintCallable)
	int32 GetItemCount(int32 id) const;
	void UpdateItemCount(int32 id, int32 value);
	//Get difference between value and current item count
	int32 GetItemCountDifference(int32 id, int32 value) const;
};
