// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Interactable.generated.h"

UCLASS()
class OCTOPUS_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;	
	
	
	virtual void Interact(class AMyPlayerController* pController);
	UFUNCTION(BlueprintImplementableEvent)
	void InteractBP(AMyPlayerController* pController);

	UFUNCTION(BlueprintCallable)
	virtual FString GetPreInteractText() const { return FString(); }
	UFUNCTION(BlueprintCallable)
	virtual FString GetPostInteractText() const { return FString(); }

	

	UFUNCTION()
	virtual void OnPlayerEnterCollisionBox(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	virtual void OnPlayerLeaveCollisionBox(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


protected:
	virtual void OnPlayerEnterCollisionBox(AMyPlayerController* pController) {}
	virtual void OnPlayerLeaveCollisionBox() {}


	UPROPERTY(EditAnywhere)
		USceneComponent* Root;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere)
		UShapeComponent* CollisionBox;
};
