// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "Door.generated.h"

/**
 * 
 */
UCLASS()
class OCTOPUS_API ADoor : public AInteractable
{
	GENERATED_BODY()
	
public:
	ADoor();

	UFUNCTION(BlueprintCallable)
		virtual FString GetPostInteractText() const override;

	UFUNCTION(BlueprintImplementableEvent)
		void Open();

protected:
	virtual void OnPlayerEnterCollisionBox(class AMyPlayerController* pController) override;
	virtual void OnPlayerLeaveCollisionBox() override;

	UPROPERTY(EditAnywhere)
		int32 NeededItemId;

	UPROPERTY(EditAnywhere)
		int32 Count;

	UPROPERTY(EditAnywhere)
		FString Name;

	AMyPlayerController* CurrentPlayer;

};
