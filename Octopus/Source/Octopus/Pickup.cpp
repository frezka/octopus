// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickup.h"
#include "Octopus.h"
#include "MyPlayerController.h"


APickup::APickup()
{
	Id = 0;
	Count = 1;
}

FString APickup::GetPreInteractText() const
{
	return FString::Printf(TEXT("%s. Press 'E' to pickup."), *Name);
}

void APickup::Interact(AMyPlayerController* pController)
{
	pController->UpdateItemCount(Id, Count);
	AInteractable::Interact(pController);
}
